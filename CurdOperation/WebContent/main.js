function OnRegistrationClick(){

	window.location.href="registration.jsp";
}


function registerUser(){

	var cpassword = document.getElementById("cpassword").value;
	var fname = document.getElementById("fname").value;
	var mname = document.getElementById("mname").value;
	var lname = document.getElementById("lname").value;
	var mobileNo = document.getElementById("mobileNo").value;
	var email = document.getElementById("email").value;
	var password = document.getElementById("password").value;
	/*
	 */
	var user = {"fname":fname,"mname":mname,"lname":lname,"mobileNo":mobileNo,"email":email}

	var flag = validateUser(user);

	if(flag==false){
		return false;
	}
	if (password.length<8){
		document.getElementById("errorpassword").innerHTML ="Password must be at least 8 characters long";
		document.getElementById("errorpassword").style.color = "red"

			//alert("Password must be at least 8 characters long")
			return false;
	}
	if (cpassword.length<8){
		document.getElementById("errorcpassword").innerHTML ="Confirm Password must be at least 8 characters long";
		document.getElementById("errorcpassword").style.color = "red"

			//alert("Password must be at least 8 characters long")
			return false;
	}
	if(password!=cpassword){
		alert("password and cpassword must be same")
		return false;
	}

	generateTable(user);

}


//this function is used to validate user

function validateUser(user){

	var flag=true;

	if(user.fname==""){

		document.getElementById("errorfname").innerHTML = "first name cannot be empty";
		document.getElementById("errorfname").style.color = "red"
			//alert("Name cannot be blank");
			flag=false;
		return flag;
	}
	if(user.lname==""){
		document.getElementById("errorlname").innerHTML ="last name cannot be empty";
		document.getElementById("errorlname").style.color = "red"
			//alert("Name cannot be blank");
			flag=false;
		return flag;
	}
	if(isNaN(user.mobileNo)){
		alert("mobile no must be digit")
		if(user.mobileNo==""){
			document.getElementById("errormobileNo").innerHTML ="mobile no cannot be empty";
			document.getElementById("errormobileNo").style.color = "red"

				//alert("Name cannot be blank");
				flag=false;
			return flag;
		}

		flag=false;
		return flag;

	}

	if(parseInt(user.mobileNo.length) != 10){
		alert("mobile no must be 10 digit")
		flag=false;
		return flag;
	}
	if(user.email==""){
		document.getElementById("erroremail").innerHTML ="email-id cannot be empty";
		document.getElementById("erroremail").style.color = "red"
			//alert("Name cannot be blank");
			flag=false;
		return flag;
	}

}

var index = 0;
function generateTable(user){
	console.log(user);

	index++;

	var usertable = document.getElementById('tblData');


	var tbody = document.getElementById('tblDatatbody');


	var row=usertable.insertRow(usertable.rows.length);


	var cell1=row.insertCell(0);

	cell1.innerHTML=index;

	var cell2=row.insertCell(1);
	cell2.innerHTML=user.fname;

	var cell3=row.insertCell(2);
	cell3.innerHTML=user.mname;


	var cell4=row.insertCell(3);
	cell4.innerHTML=user.lname;

	var cell5=row.insertCell(4);
	cell5.innerHTML=user.email;

	var cell6=row.insertCell(5);
	cell6.innerHTML=user.mobileNo;

	var cell7=row.insertCell(6)
	cell7.innerHTML="<input type=button value=Edit onclick=clickOnEditBtn("+index+")>"

	var cell8=row.insertCell(7)
	cell8.innerHTML="<input type=button value=Remove onclick=deleteTableRow("+index+")>"


		row.appendChild(cell1);
	row.appendChild(cell2);
	row.appendChild(cell3);
	row.appendChild(cell4);
	row.appendChild(cell5);
	row.appendChild(cell6);

	row.appendChild(cell7);
	row.appendChild(cell8);
	tbody.appendChild(row)
	
	
	document.getElementById("tbldiv").style.display="block";
	console.log(row);
}

//function is used to clear all the fields
function clearFields(){
	// alert("called")
	document.getElementById("fname").value="";
	document.getElementById("mname").value="";
	document.getElementById("lname").value="";
	document.getElementById("mobileNo").value="";
	document.getElementById("email").value="";
	document.getElementById("password").value="";
	document.getElementById("cpassword").value="";

}


var value = 0;
function clickOnEditBtn(index){
	document.getElementById("updDiv").style.display='block';

	document.getElementById("divUser").style.display='none';

	var tbody = document.getElementById('tblDatatbody');

	var fname ="";
	var mname ="";
	var lname ="";
	var email ="";
	var mobile ="";


	//gets rows of table
	var rowLength = tbody.rows.length;

	for (i = 0; i < rowLength; i++){

		//gets cells of current row
		var cells = tbody.rows.item(i).cells;

		value = cells.item(0).innerHTML;
		fname = cells.item(1).innerHTML;
		mname = cells.item(2).innerHTML;
		lname = cells.item(3).innerHTML;
		email = cells.item(4).innerHTML;
		mobile = cells.item(5).innerHTML;

	}



	document.getElementById("updfname").value=fname;
	document.getElementById("updmname").value=mname;
	document.getElementById("updlname").value=lname;
	document.getElementById("updmobileNo").value=mobile;
	document.getElementById("updemail").value=email;


}


//function is used to update the information of user
function updateUser(){


	var fname = document.getElementById("updfname").value;
	var mname = document.getElementById("updmname").value;
	var lname = document.getElementById("updlname").value;
	var mobileNo = document.getElementById("updmobileNo").value;
	var email = document.getElementById("updemail").value;

	var user = {"fname":fname,"mname":mname,"lname":lname,"mobileNo":mobileNo,"email":email}

	validateUser(user);
	
	updateTable(user);
}

function updateTable(user){
	
	
	var tbody = document.getElementById('tblDatatbody');
	
	var rowlength = tbody.rows.length;
	
	if(rowlength>0){
		var id = value;
		id--;
		for (i = 0; i < rowlength; i++){

			//gets cells of current row
			var cells = tbody.rows.item(id).cells;

			var indx = cells.item(0).innerHTML;
			
			console.log(indx,value)
			if(indx == value){
			
			//	for (i = 0; i < rowLength; i++){

					//gets cells of current row
					var cells = tbody.rows.item(0).cells;

				    cells.item(0).innerHTML=value;
					cells.item(1).innerHTML=user.fname;
					cells.item(2).innerHTML=user.mname;
					cells.item(3).innerHTML=user.lname;
					cells.item(4).innerHTML=user.email;
					cells.item(5).innerHTML=user.mobileNo;

				//}
			}
			
		}
		
	}
	
}


function deleteTableRow(index){
	index--;
	 document.getElementById("tblDatatbody").deleteRow(index);
	
}
