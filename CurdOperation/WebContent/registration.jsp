<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registraion</title>
<script type="text/javascript" src="main.js">
</script>
<style type="text/css">
#fname{ 
        margin-left:50px
}
#mname{ 
        margin-left:34px
}
#lname{ 
        margin-left:50px
}
#mobileNo{ 
        margin-left:16px
}
#email{ 
        margin-left:60px
}
#password{ 
        margin-left:55px
}

#updfname{ 
        margin-left:50px
}
#updmname{ 
        margin-left:34px
}
#updlname{ 
        margin-left:50px
}
#updmobileNo{ 
        margin-left:16px
}
#updemail{ 
        margin-left:60px
}
</style>

</head>
<body>
<form  action="login" method="post">

<div id="divUser">
<h1>User Registration</h1>

*First Name<input  type="text" id="fname" name="fname">&nbsp;<label id="errorfname"></label><br><br>

Middle Name<input type="text" id="mname" name="mname"><br><br>

*Last Name<input type="text"  id="lname" name="lname">&nbsp;<label id="errorlname"></label><br><br>

*Mobile Number<input type="text"  id="mobileNo" name="mobileNo">&nbsp;<label id="errormobileNo"></label><br><br>

*Email-id<input type="email" id="email" name="email">&nbsp;<label id="erroremail"></label><br><br>

*Password<input type="password" id="password"  name="password">&nbsp;<label id="errorpassword"></label><br><br>

*Confirm Password<input type="password"  id="cpassword" name="cpassword">&nbsp;<label id="errorcpassword"></label><br><br>

<input type="button" value="Save"  onclick="registerUser()">

<input type="button" value="Clear" onclick="clearFields()">
</div>
</form>

<form action="updateUser">
<div style="display:none" id="updDiv">
<h1>Edit Information</h1>

*First Name<input  type="text" id="updfname" name="fname">&nbsp;<label id="errorfname"></label><br><br>

Middle Name<input type="text" id="updmname" name="mname"><br><br>

*Last Name<input type="text"  id="updlname" name="lname">&nbsp;<label id="errorlname"></label><br><br>

*Mobile Number<input type="text"  id="updmobileNo" name="mobileNo">&nbsp;<label id="errormobileNo"></label><br><br>

*Email-id<input type="email" id="updemail" name="email">&nbsp;<label id="erroremail"></label><br><br>


<input type="button" value="Update"  onclick="updateUser()">

<input type="button" value="Clear" onclick="clearFields()">
</div>
</form>
<br>
<br>
<div id="tbldiv" style="display:none">
<table border="1" id="tblData">
<tr>
<th>Id</th>
<th>First Name</th>
<th>Middle Name</th>
<th>Last Name</th>
<th>Email</th>
<th>Mobile</th>
<th>Edit</th>
<th>Remove</th>
</tr>
<tbody id="tblDatatbody"></tbody>
</table>

</div>
</body>
</html>