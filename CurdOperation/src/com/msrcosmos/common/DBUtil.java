package com.msrcosmos.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * this class is used to get the connection
 * @since 28/8/2019
 */
public class DBUtil {
	static Connection con=null;
	

	public static Connection getConnection() {
		try {
			Class.forName("oracle.jdbc.driver.OracelDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","system","sarika");
			
			if(con==null) {
				System.out.println("connection is not established");
			}
			else {
				System.out.println("connection established");
			}
		}
		catch(SQLException|ClassNotFoundException e) {
			
		}
		return con;
		
	}

}
